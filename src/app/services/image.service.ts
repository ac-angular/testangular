import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
image = {
  'code':  "SK-IMG0096",
  'id': 35,
  'name': "Logo Monty PNG",
  'url': "https://s3montygfy.s3.eu-west-1.amazonaws.com/shere-khan/images/general/monty-global-payments.png"
};
  constructor() { }

  getImageUrlByCode(code: string): string {
    return this.image.url + '?r=' + Math.floor(Math.random()*100000);;
}

  convertImgUrlToBase64(url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        let xhRequest = new XMLHttpRequest();
        xhRequest.onload = function () {
          let reader = new FileReader();
          reader.onloadend = function () {
            resolve(reader.result);
          };
          reader.readAsDataURL(xhRequest.response);
        };
        xhRequest.open('GET', url);
        xhRequest.responseType = 'blob';
        xhRequest.send();
      } catch (exception) {
        reject(exception);
      }
    });
  }



}
