import { Component } from '@angular/core';
import html2canvas from 'html2canvas';
import { ImageService } from './services/image.service';
import { jsPDF } from "jspdf";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'test-angular';
  logoBase64: string = '';

  constructor(private _imageService: ImageService) {

  }
  downloadPrecios(): void {
    // const logoUrl = this._imageService.getImageUrlByCode('SK-IMG0096');
    // this.downloadImage(logoUrl, 'prueba');
    
    setTimeout(() => {
      var data: any = document.getElementById('print-container-precios-id');
      html2canvas(data, {
        scale: 2,
        logging: true,
        allowTaint: true,
        useCORS: true,
      }).then(canvas => {
        var imgWidth = 447;
        var imgHeight = (canvas.height * imgWidth) / canvas.width;
        const contentDataURL = canvas.toDataURL('image/jpeg');
        var doc = new jsPDF('p', 'px', 'a4');
        doc.addImage(contentDataURL, 'JPEG', 0, 0, imgWidth, imgHeight);
        doc.save(`Precios_${new Date().getTime()}.pdf`);
      });
    });
    
    // this._imageService.convertImgUrlToBase64(logoUrl).then(
    //   (base64: any): any => {
    //     this.logoBase64 = base64;
    // setTimeout(() => {
    //   var data: any = document.getElementById('print-container-precios-id');
    //   html2canvas(data, {
    //     scale: 2,
    //     logging: true,
    //     allowTaint: true,
    //     useCORS: true,
    //   }).then(canvas => {
    //     var imgWidth = 447;
    //     var imgHeight = (canvas.height * imgWidth) / canvas.width;
    //     const contentDataURL = canvas.toDataURL('image/jpeg');
    //     var doc = new jsPDF('p', 'px', 'a4');
    //     doc.addImage(contentDataURL, 'JPEG', 0, 0, imgWidth, imgHeight);
    //     doc.save(`Precios_${new Date().getTime()}.pdf`);
    //   });
    // });
    //   },
    //   error => {
    //     // Tratamiento del error
    //   }
    // );
  }

  downloadImage(url: string, name: string) {
    fetch(url)
      .then(resp => resp.blob())
      .then(blob => {
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.style.display = 'none';
        a.href = url;
        // the filename you want
        a.download = name;
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
      })
      .catch(() => alert('An error sorry'));
  }

 
}
