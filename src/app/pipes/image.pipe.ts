import { Pipe, PipeTransform } from '@angular/core';
import { ImageService } from 'src/app/services/image.service';

@Pipe({
  name: 'image',
})
export class ImagePipe implements PipeTransform {
  constructor(private imageService: ImageService) {}

  transform(value: string): string {
    return this.imageService.getImageUrlByCode(value);
  }
}
